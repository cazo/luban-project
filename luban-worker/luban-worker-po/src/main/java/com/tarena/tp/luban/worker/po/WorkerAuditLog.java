package com.tarena.tp.luban.worker.po;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name = "worker_audit_log")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkerAuditLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(11)")
    /**
     * 主键
     */
     Long id;

    /**
     * Passport 用户ID
     */
    @Column(name = "user_id", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT 'Passport 用户ID'", updatable = false)
     Long userId;

    /**
     * 用户名称
     */
    @Column(name="user_name")
    String userName;

    /**
     * 师傅 ID
     */
    @Column(name = "worker_id")
    Long workerId;

    /**
     * 操作时间
     */
    @Column(name="operate_time")
    Long operateTime;

    /**
     * 操作步骤
     */
    @Column(name="operate_name")
    String operateName;

    /**
     * 审核状态 0:驳回，1:通过  2:未审核
     */
    @Column(name="audit_status")
    Integer auditStatus;

    /**
     * 驳回原因
     */
    @Column(name="reject_reason")
    String rejectReason;

    /**
     * 备注
     */
    @Column(name="remark")
    String remark;

    /**
     * 创建人名称
     */
    @Column(name = "create_user_name")
    private String createUserName;
    /**
     * 创建人ID
     */
    @Column(name = "create_user_id")
    private Long createUserId;
    /**
     * 修改人ID
     */
    @Column(name = "modified_user_id")
    private Long modifiedUserId;
    /**
     * 修改人姓名
     */
    @Column(name = "modified_user_name")
    private String modifiedUserName;
    /**
     * 创建时间
     */
    @Column(name = "gmt_create")
    private Long gmtCreate;
    /**
     * 更新时间
     */
    @Column(name = "gmt_modified")
    private Long gmtModified;


}
