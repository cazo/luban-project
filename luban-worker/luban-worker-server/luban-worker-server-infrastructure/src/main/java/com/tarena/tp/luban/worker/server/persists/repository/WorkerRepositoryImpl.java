package com.tarena.tp.luban.worker.server.persists.repository;


import com.tarena.tp.luban.worker.server.bo.WorkerBO;
import com.tarena.tp.luban.worker.server.dao.WorkerDAO;
import com.tarena.tp.luban.worker.server.dto.param.WorkerCreateParam;

import com.tarena.tp.luban.worker.server.persists.converter.WorkerConverter;
import com.tarena.tp.luban.worker.server.repository.WorkerRepository;
import lombok.extern.slf4j.Slf4j;

import com.tarena.tp.luban.worker.po.Worker;
import com.tedu.inn.protocol.exception.BusinessException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


@Component
@Slf4j
public class WorkerRepositoryImpl implements WorkerRepository {

    @Resource
    private WorkerConverter workerConverter;

    @Resource
    private WorkerDAO workerDAO;

    @Override
    public Long save(WorkerCreateParam workerCreateParam) {
        Worker worker = this.workerConverter.param2po(workerCreateParam);
        if (worker.getId() != null) {
            this.workerDAO.update(worker);
            return worker.getId();
        }
        this.workerDAO.insert(worker);
        return worker.getId();
    }

    @Override
    public WorkerBO getWorker(Long workerId) throws BusinessException{
        //根据 师傅 ID 查询用户信息
        Worker worker = workerDAO.getWorkerByWorkerId(workerId);
        return workerConverter.po2bo(worker);
    }

    @Override
    public void delete(Long userId) {
        workerDAO.delete(userId);
    }

}
