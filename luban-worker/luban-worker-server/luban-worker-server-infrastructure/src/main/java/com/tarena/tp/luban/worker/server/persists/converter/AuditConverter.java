package com.tarena.tp.luban.worker.server.persists.converter;

import com.tarena.tp.luban.worker.po.WorkerAuditLog;
import com.tarena.tp.luban.worker.server.bo.AuditBO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class AuditConverter {

    public AuditBO po2bo(WorkerAuditLog auditLog) {
        AuditBO auditBO = new AuditBO();
        BeanUtils.copyProperties(auditLog, auditBO);
        return auditBO;
    }

    public List<AuditBO> pos2bos(List<WorkerAuditLog> auditLog) {
        if (CollectionUtils.isEmpty(auditLog)) {
            return Collections.emptyList();
        }
        List<AuditBO> result = new ArrayList<>();
        for (WorkerAuditLog log : auditLog) {
            result.add(po2bo(log));
        }
        return result;
    }
}
