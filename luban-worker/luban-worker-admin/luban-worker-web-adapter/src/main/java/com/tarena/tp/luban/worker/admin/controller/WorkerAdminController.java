package com.tarena.tp.luban.worker.admin.controller;

import com.tarena.tp.luban.worker.admin.assemble.WorkerAssemble;
import com.tarena.tp.luban.worker.admin.assemble.WorkerAuditAssemble;
import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.manager.WorkerAdminService;
import com.tarena.tp.luban.worker.admin.protocol.param.AuditParam;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;
import com.tarena.tp.luban.worker.admin.protocol.vo.AuditDetailVO;
import com.tarena.tp.luban.worker.admin.protocol.vo.WorkerVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.pager.PagerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WorkerAdminController {
    @Autowired
    private WorkerAdminService workerAdminService;
    @Autowired
    private WorkerAssemble workerAssemble;
    @Autowired
    private WorkerAuditAssemble workerAuditAssemble;
    /**
     * 师傅审核申请列表查询
     */
    @PostMapping("/admin/worker/aduit")
    public PagerResult<WorkerVO> queryAuditWorkers(@RequestBody WorkerQuery workerQuery){
        ListRecordTotalBO<WorkerBO> workerBOS=
                workerAdminService.queryAuditWorkers(workerQuery);
        return workerAssemble.assemblePagerResult(workerBOS,workerQuery);
    }
    /**
     * 审核详情查询
     */
    @PostMapping("/admin/worker/audit/detail")
    public AuditDetailVO auditDetail(@RequestParam("workerId") Long userId) throws BusinessException {
        //调用业务方法获取workerBO
        WorkerBO workerBO=workerAdminService.auditDetail(userId);
        //返回的时候转化成auditVO
        return workerAuditAssemble.assembleBO2DetailVO(workerBO);
    }
    /**
     * 审核提交
     */
    @PostMapping("/admin/worker/audit/save")
    public Long auditSave(@RequestBody AuditParam auditParam) throws BusinessException{
        return workerAdminService.auditSave(auditParam);
    }
}
