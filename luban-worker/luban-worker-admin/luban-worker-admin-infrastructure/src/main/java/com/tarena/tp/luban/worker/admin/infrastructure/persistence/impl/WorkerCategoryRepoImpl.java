package com.tarena.tp.luban.worker.admin.infrastructure.persistence.impl;

import com.tarena.tp.luban.worker.admin.bo.WorkerCategoryBO;
import com.tarena.tp.luban.worker.admin.dao.WorkerCategoryDAO;
import com.tarena.tp.luban.worker.admin.infrastructure.persistence.data.converter.WorkerCategoryConverter;
import com.tarena.tp.luban.worker.admin.repository.WorkerCategoryRepository;
import com.tarena.tp.luban.worker.admin.repository.WorkerRepository;
import com.tarena.tp.luban.worker.po.WorkerCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class WorkerCategoryRepoImpl implements WorkerCategoryRepository {
    @Autowired
    private WorkerCategoryDAO workerCategoryDAO;
    @Autowired
    private WorkerCategoryConverter workerCategoryConverter;
    @Override
    public List<WorkerCategoryBO> getWorkerCategory(Long userId) {
        List<WorkerCategory> categoryByUserId = workerCategoryDAO.getCategoryByUserId(userId);

        return workerCategoryConverter.poList2BoList(categoryByUserId);
    }
}
