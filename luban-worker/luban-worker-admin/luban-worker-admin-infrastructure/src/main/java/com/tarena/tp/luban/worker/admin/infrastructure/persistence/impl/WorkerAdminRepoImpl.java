package com.tarena.tp.luban.worker.admin.infrastructure.persistence.impl;

import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.dao.WorkerDAO;
import com.tarena.tp.luban.worker.admin.dao.query.WorkerDBPagerQuery;
import com.tarena.tp.luban.worker.admin.infrastructure.persistence.data.converter.WorkerConverter;
import com.tarena.tp.luban.worker.admin.protocol.param.WorkerParam;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;
import com.tarena.tp.luban.worker.admin.repository.WorkerRepository;
import com.tarena.tp.luban.worker.po.Worker;
import com.tedu.inn.protocol.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WorkerAdminRepoImpl extends CacheRepository<WorkerBO> implements WorkerRepository {
    @Autowired(required = false)
    private WorkerDAO workerDAO;
    @Autowired
    private WorkerConverter workerConverter;
    @Override
    public WorkerBO getWorker(Long userId) throws BusinessException {
        //利用参数查询
        Worker worker = workerDAO.getEntity(userId);
        //转化返回
        return workerConverter.po2bo(worker);
    }

    @Override
    public Long getWorkerCount(WorkerQuery workerQuery) {
        //select count(*) from worker where 条件
        //转化
        WorkerDBPagerQuery dbPager = workerConverter.toDbPagerQuery(workerQuery);
        //调用方法
        return workerDAO.countWorker(dbPager);
    }

    @Override
    public List<WorkerBO> queryWorkers(WorkerQuery workerQuery) {
        //转化
        WorkerDBPagerQuery dbPager = workerConverter.toDbPagerQuery(workerQuery);
        //查询
        List<Worker> workers=workerDAO.queryWorkers(dbPager);
        //转
        return workerConverter.poList2BoList(workers);
    }

    @Override
    public Integer disable(Long bankIds) {
        return null;
    }

    @Override
    public Integer enable(Long bankIds) {
        return null;
    }

    @Override
    public void updateAuditStatus(WorkerParam workerParam) {
        // 针对 前台缓存 做双删
        String key="luban:worker:"+workerParam.getUserId();
        del(key);
        //param转化po
        Worker worker = workerConverter.param2po(workerParam);
        workerDAO.updateWorkerAuditStatus(worker);
        //延迟 2秒
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        del(key);
    }
    @Override
    public void updateCertStatus(WorkerParam workerParam) {
        //延迟双删
        String key="luban:worker:"+workerParam.getUserId();
        del(key);
        Worker worker = workerConverter.param2po(workerParam);
        //UPDATE WORKER SET CERT_STATUS = 1 , MODIFIED_TIME=... WHERE USER_ID=
        workerDAO.updateWorkerCertStatus(worker);
        //延迟 2秒
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        del(key);
    }
}
