package com.tarena.tp.luban.worker.admin.infrastructure.persistence.impl;

import com.tarena.tp.luban.worker.admin.bo.AuditBO;
import com.tarena.tp.luban.worker.admin.dao.AuditDAO;
import com.tarena.tp.luban.worker.admin.infrastructure.persistence.data.converter.AuditConverter;
import com.tarena.tp.luban.worker.admin.protocol.param.AuditParam;
import com.tarena.tp.luban.worker.admin.repository.AuditLogRepository;
import com.tarena.tp.luban.worker.po.WorkerAuditLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class AuditLogRepoImpl implements AuditLogRepository {
    @Autowired(required = false)
    private AuditDAO auditDAO;
    @Autowired
    private AuditConverter auditConverter;
    @Override
    public Long save(AuditParam auditParam) {
        //param转成po
        WorkerAuditLog workerAuditLog = auditConverter.param2po(auditParam);
        workerAuditLog.setUserId(auditParam.getWorkerId());
        workerAuditLog.setWorkerId(null);
        //dao持久层新增
        auditDAO.insert(workerAuditLog);
        return workerAuditLog.getId();//mybatis框架给对象 补充的id
    }

    @Override
    public List<AuditBO> getAudit(Long userId) {
        List<WorkerAuditLog> auditLog = auditDAO.getAuditLog(userId);
        return auditConverter.pos2bos(auditLog);
    }
}
