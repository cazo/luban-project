package com.tarena.tp.luban.worker.admin.manager;

import com.tarena.tp.attach.server.client.AttachApi;
import com.tarena.tp.attach.server.dto.AttachDTO;
import com.tarena.tp.attach.server.query.AttachQuery;
import com.tarena.tp.luban.account.server.client.AccountApi;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.worker.admin.bo.AuditBO;
import com.tarena.tp.luban.worker.admin.bo.WorkerAreaBO;
import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.bo.WorkerCategoryBO;
import com.tarena.tp.luban.worker.admin.protocol.param.AuditParam;
import com.tarena.tp.luban.worker.admin.protocol.param.WorkerParam;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;
import com.tarena.tp.luban.worker.admin.repository.AuditLogRepository;
import com.tarena.tp.luban.worker.admin.repository.WorkerAreaRepository;
import com.tarena.tp.luban.worker.admin.repository.WorkerCategoryRepository;
import com.tarena.tp.luban.worker.admin.repository.WorkerRepository;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class WorkerAdminService {
    @Autowired
    private WorkerRepository workerRepository;
    @Autowired(required = false)
    private WorkerAreaRepository workerAreaRepository;
    @Autowired(required = false)
    private WorkerCategoryRepository workerCategoryRepository;
    @Autowired
    private AuditLogRepository auditLogRepository;
    public ListRecordTotalBO<WorkerBO> queryAuditWorkers(WorkerQuery workerQuery) {
        // 目标: 查询auditStatus 0,2  封装数据的属性 total 总条数 list 分页数据
        //1. 查询 总条数 select count from worker where and and and...
        List<Integer> auditStatus = workerQuery.getAuditStatus();
        if (CollectionUtils.isEmpty(auditStatus)){
            // 补充0,2 默认值
            auditStatus.add(0);
            auditStatus.add(2);
        }
        //仓储层接口没实现
        Long total = workerRepository.getWorkerCount(workerQuery);
        List<WorkerBO> workerBOList=null;
        //2. 判断总条数是否大于0
        if (total>0){
            //2.1 大于0条件 封装完整所需的数据
            //继续查询 然后封装完整数据
            //workerQuery的特殊auditStatus 补充条件
            //仓储层接口没实现
            workerBOList= workerRepository.queryWorkers(workerQuery);
            //防止前端表单读取不到数据
            for (WorkerBO workerBO : workerBOList) {
                //UNDO 缺少area和category的信息 但是列表可以没有这些数据 在详情补充
                workerBO.setAreaDetails(new ArrayList<>());
                workerBO.setCategoryDetails(new ArrayList<>());
            }
        }
        //assemble对于null是否有处理,避免出现代码逻辑错误导致的空指针
        return new ListRecordTotalBO<>(workerBOList,total);
    }
    @Autowired
    private AttachApi attachApi;
    public WorkerBO auditDetail(Long userId) throws BusinessException {
        //目标: 通过查询 dubbo调用 补充所有数据 包括 worker area category audit_log  attach-server
        //1.查询worker
        WorkerBO worker = workerRepository.getWorker(userId);
        if (worker!=null&&worker.getId()!=null){
            //2.在不为空的情况下,才去查询worker对应绑定 area category 日志
            //封装 area
            List<WorkerAreaBO> workerAreas = workerAreaRepository.getWorkerArea(userId);
            List<String> areaDetails=new ArrayList<>();
            if (!CollectionUtils.isEmpty(workerAreas)){
                for (WorkerAreaBO workerArea : workerAreas) {
                    String areaDetail=workerArea.getAreaDetail();
                    areaDetails.add(areaDetail);
                }
            }
            worker.setAreaDetails(areaDetails);
            //封装 category
            List<WorkerCategoryBO> workerCategories = workerCategoryRepository.getWorkerCategory(userId);
            List<String> catsDetials=new ArrayList<>();
            if (!CollectionUtils.isEmpty(workerCategories)){
                for (WorkerCategoryBO workerCategory : workerCategories) {
                    String catDetail=workerCategory.getCategoryDetail();
                    catsDetials.add(catDetail);
                }
            }
            worker.setCategoryDetails(catsDetials);
            //封装 审核日志
            List<AuditBO> audit = auditLogRepository.getAudit(userId);
            worker.setWorkerAuditLogs(audit);
            //封装 url图片连接地址
            //3.在不为空的情况下,才封装attachInfo
            List<String> urls=getAttachList(100,worker.getId());
            worker.setAttachInfo(urls);
        }
        return worker;
    }

    private List<String> getAttachList(int businessType, Long id) {
        AttachQuery attachQuery=new AttachQuery();
        attachQuery.setBusinessType(businessType);
        attachQuery.setBusinessId(id.intValue());
        List<AttachDTO> attachInfoByParam = attachApi.getAttachInfoByParam(attachQuery);
        //urlPrefix+fileUuid
        List<String> urls=new ArrayList<>();
        if (!CollectionUtils.isEmpty(attachInfoByParam)){
            for (AttachDTO attachDTO : attachInfoByParam) {
                String url="http://localhost:8092/static/"+attachDTO.getFileUuid();
                urls.add(url);
            }
        }
        return urls;
    }
    @Autowired
    private AccountApi accountApi;
    public Long auditSave(AuditParam auditParam) throws BusinessException {
        //1.将记录 写入到数据库 worker_audit_log
        Long auditLogId = auditLogRepository.save(auditParam);
        //2.修改 worker表中 师傅的审核状态 缓存
        Long userId = auditParam.getWorkerId();
        Integer auditStatus = auditParam.getAuditStatus();
        //update worker set auditStatus=1|0 where user_id=#{userId}
        WorkerParam workerParam=new WorkerParam();
        workerParam.setUserId(userId);
        workerParam.setAuditStatus(auditStatus);
        //仓储层实现
        workerRepository.updateAuditStatus(workerParam);
        //3.判断 当前师傅是否存在 平台账号 cert_status 如果是通过审核 要创建平台账号
        //由于师傅 或者用户的平台账号 有可能从别的接口功能 创建 当前师傅cert_status
        WorkerBO worker = workerRepository.getWorker(userId);
        if (worker.getCertStatus()==0 && auditParam.getAuditStatus()==1){
            //远程调用account创建账号
            System.out.println("当前注入的accountApi实际上是代理对象:"+accountApi.getClass().getName());
            AccountParam accountParam=new AccountParam();
            accountParam.setUserId(worker.getUserId());
            accountParam.setUserName(worker.getRealName());
            accountParam.setUserPhone(worker.getPhone());
            accountApi.create(accountParam);
            //创建完了 本地worker表格更新certStatus
            workerParam.setCertStatus(1);//从0更新成1
            workerRepository.updateCertStatus(workerParam);
        }
        return auditLogId;
    }
}
