package com.tarena.tp.luban.worker.server.common.util;

import org.apache.http.client.utils.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateUtil {

    public static String getDateStr(Date date, String formatType) {
        SimpleDateFormat format = new SimpleDateFormat(formatType);
        return format.format(date);
    }

}
