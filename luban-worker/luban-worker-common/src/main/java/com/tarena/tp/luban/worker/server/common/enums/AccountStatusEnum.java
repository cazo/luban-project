package com.tarena.tp.luban.worker.server.common.enums;

public enum AccountStatusEnum {

    ENABLE(1, "启用"),
    DISABLE(0,"未启用");

    AccountStatusEnum(Integer value, String message) {
        this.value = value;
        this.message = message;
    }

    private Integer value;

    private String message;

    public Integer getValue() {
        return this.value;
    }
}
