package com.tarena.tp.luban.worker.server.common.enums;

/**
 * 账号状态枚举类
 */
public enum StatusEnum {
    LOCKED(0, "锁定"),
    NORMAL(1, "正常");

    private int value;
    private String description;

    StatusEnum(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}

