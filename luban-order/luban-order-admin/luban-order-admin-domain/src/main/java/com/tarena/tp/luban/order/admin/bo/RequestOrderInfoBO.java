package com.tarena.tp.luban.order.admin.bo;


import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestOrderInfoBO {

    String requestOrderNo;

    String requestOrderProvider;

    String requestOrderType;

    Long requestOrderPrice;

    Long gmtCreate;


}
