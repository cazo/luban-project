package com.tarena.tp.luban.order.admin.bo;


import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderInfoBO {

    String orderNo;

    Integer status;

    Long gmtCreate;

    Long gmtFinish;
}
