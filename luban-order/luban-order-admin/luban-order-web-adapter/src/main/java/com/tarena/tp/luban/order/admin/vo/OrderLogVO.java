package com.tarena.tp.luban.order.admin.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderLogVO {

    @ApiModelProperty("操作时间")
    Long operateTime;

    @ApiModelProperty("操作步骤")
    String operateName;

    @ApiModelProperty("备注")
    String remark;

    @ApiModelProperty("订单编号")
    String orderNo;

}
