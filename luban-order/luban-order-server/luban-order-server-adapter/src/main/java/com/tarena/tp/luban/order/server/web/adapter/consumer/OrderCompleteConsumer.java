package com.tarena.tp.luban.order.server.web.adapter.consumer;

import com.tarena.tp.luban.order.server.domain.manager.OrderServerService;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic="order_complete_topic",consumerGroup = "order_complete_group")
public class OrderCompleteConsumer implements RocketMQListener<String> {
    @Autowired
    private OrderServerService orderServerService;
    @Override
    public void onMessage(String message) {
        orderServerService.complete(message);
    }
}
