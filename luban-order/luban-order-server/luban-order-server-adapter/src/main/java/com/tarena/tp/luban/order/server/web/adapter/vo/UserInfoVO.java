package com.tarena.tp.luban.order.server.web.adapter.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserInfoVO {

    @ApiModelProperty("用户姓名")
    private
    String userName;

    @ApiModelProperty("用户电话")
    private
    String userPhone;

    @ApiModelProperty("用户地址")
    private
    String userAddress;

}
