package com.tarena.tp.luban.order.server.web.adapter.consumer;

import com.tarena.tp.luban.order.server.domain.manager.OrderServerService;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic="order_delay_topic",consumerGroup = "order_delay_group")
public class OrderDelayConsumer implements RocketMQListener<String> {
    @Autowired
    private OrderServerService orderServerService;
    @Override
    public void onMessage(String orderNo) {
        orderServerService.delay(orderNo);
    }
}
