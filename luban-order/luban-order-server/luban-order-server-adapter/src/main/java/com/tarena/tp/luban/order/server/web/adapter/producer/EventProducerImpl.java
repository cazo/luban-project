package com.tarena.tp.luban.order.server.web.adapter.producer;

import com.tarena.tp.luban.order.server.domain.manager.EventService;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class EventProducerImpl implements EventService {
    //消息结算 主题
    private static final String ORDER_SETTLE_TOPIC="order_settle_topic";
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Override
    public void sendOrderFinishEvent(OrderMqDTO orderMq) {
        //封装消息
        Message<OrderMqDTO> message = MessageBuilder.withPayload(orderMq).build();
        //调用asyncSend发送消息 syncSend
        rocketMQTemplate.asyncSend(ORDER_SETTLE_TOPIC, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("消息:"+orderMq+"发送成功");
            }

            @Override
            public void onException(Throwable e) {
                //如果异常可以在回调拿到异常结果 做补偿操作.
                System.out.println("消息:"+orderMq+"发送失败,请人工处理");
            }
        });
    }

    @Override
    public void sendOrderCancelEvent(OrderMqDTO orderMqDTO) {

    }

    @Override
    public void sendDelaySignEvent(String orderNo) {
        Message<String> message = MessageBuilder.withPayload(orderNo).build();
        rocketMQTemplate.asyncSend("order_delay_topic", message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("成功");
            }
            @Override
            public void onException(Throwable e) {
                System.out.println("失败");
            }
        },3000,4);//4表示 delayLevel 延迟30秒
    }
}
