package com.tarena.tp.luban.order.server.web.adapter.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderInfoVo {

    @ApiModelProperty("订单号")
    private
    String orderNo;

    @ApiModelProperty("创建日期")
    private
    Long createTime;

    @ApiModelProperty("订单状态")
    private
    Integer status;

    @ApiModelProperty("签到日期")
    private
    Long signTime;

    @ApiModelProperty("完成日期")
    private
    Long finishTime;


}
