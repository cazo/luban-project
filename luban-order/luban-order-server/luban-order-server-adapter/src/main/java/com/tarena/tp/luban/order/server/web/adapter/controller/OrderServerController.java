package com.tarena.tp.luban.order.server.web.adapter.controller;

import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.manager.OrderServerService;
import com.tarena.tp.luban.order.server.protocol.param.OrderConfirmParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderFinishParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderSignParam;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import com.tarena.tp.luban.order.server.web.adapter.assemble.OrderAssemble;
import com.tarena.tp.luban.order.server.web.adapter.vo.OrderDetailVO;
import com.tarena.tp.luban.order.server.web.adapter.vo.OrderVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.pager.PagerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderServerController {
    /**
     * 师傅抢单 生成订单
     */
    @Autowired
    private OrderServerService orderServerService;
    @Autowired
    private OrderAssemble orderAssemble;
    @PostMapping("/order/create")
    public String create(@RequestBody OrderParam orderParam) throws BusinessException {
        //业务层调用创建订单 返回业务层生成的订单编号
        String orderNo=orderServerService.create(orderParam);
        return orderNo;
    }
    /**
     * 师傅工作台的订单列表查询
     */
    @PostMapping("order/list")
    public PagerResult<OrderVO> orderList(@RequestBody OrderQuery orderQuery)throws BusinessException{
        //先获取一个包含total值和list值的对象
        ListRecordTotalBO<OrderBO> orderBOS=orderServerService.orderList(orderQuery);
        //转化成pagerResult返回
        return orderAssemble.assemblePagerResult(orderBOS,orderQuery);
    }
    /**
     * 列表中某个订单的详情信息
     */
    @GetMapping("/order/info")
    public OrderDetailVO detail(String orderNo) throws BusinessException{
        // 业务层返回bo
        OrderBO orderBO=orderServerService.detail(orderNo);
        // 转化vo返回
        return orderAssemble.assembleBO2DetailVO(orderBO);
    }
    /**
     * 师傅入场签到
     */
    @PostMapping("/order/sign")
    public String sign(@RequestBody OrderSignParam orderSignParam) throws BusinessException{
        return orderServerService.sign(orderSignParam);
    }
    /**
     * 确认绑定上传的图片
     */
    @PostMapping("/order/confirm")
    public String confirm(@RequestBody OrderConfirmParam orderConfirmParam) throws BusinessException{
        return orderServerService.confirm(orderConfirmParam);
    }

    /**
     * 师傅点击确认完成
     */
    @PostMapping("/order/finish")
    public String finish(@RequestBody OrderFinishParam orderFinishParam)throws BusinessException{
        return orderServerService.finish(orderFinishParam);
    }
}


















