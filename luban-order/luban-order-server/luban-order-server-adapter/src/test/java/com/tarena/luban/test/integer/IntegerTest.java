package com.tarena.luban.test.integer;

public class IntegerTest {
    public static void main(String[] args) {
        Integer a=new Integer(120);
        Integer b=new Integer(120);
        Integer c=500;
        Integer d=500;
        //integer 常量池 500不在常量池范围内
        System.out.println(a==b);
        // c单独装包的对象 d单独装包对象
        System.out.println(c==d);
    }
}
