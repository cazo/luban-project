package com.tarena.tp.luban.order.server.domain.manager;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.attach.server.client.AttachApi;
import com.tarena.tp.attach.server.dto.AttachDTO;
import com.tarena.tp.attach.server.param.AttachUpdateParam;
import com.tarena.tp.attach.server.query.AttachQuery;
import com.tarena.tp.luban.demand.server.client.RequestOrderApi;
import com.tarena.tp.luban.order.common.enums.OrderStatusEnum;
import com.tarena.tp.luban.order.common.enums.ResultEnum;
import com.tarena.tp.luban.order.server.domain.assembler.OrderAssembler;
import com.tarena.tp.luban.order.server.domain.bo.AttachInfoBO;
import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.bo.OrderLogBO;
import com.tarena.tp.luban.order.server.domain.repository.OrderLogRepository;
import com.tarena.tp.luban.order.server.domain.repository.OrderRepository;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.order.server.protocol.param.*;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class OrderServerService {
    @Autowired(required = false)
    private RequestOrderApi requestOrderApi;
    @Autowired(required = false)
    private OrderRepository orderRepository;
    @Autowired(required = false)
    private OrderLogRepository orderLogRepository;
    @Autowired(required = true)
    private OrderAssembler orderAssembler;
    public String create(OrderParam orderParam) throws BusinessException {
        //1. 根据需求单号 远程调用 进行抢单操作
        Boolean result = requestOrderApi.grabOrder(orderParam.getRequestOrderNo());
        //2.根据抢单结果,执行后续逻辑
        if (!result){
            //2.1 没抢到 抛异常返回
            throw new BusinessException(ResultEnum.ORDER_GRAB_FAIL);
        }
        //2.2 抢到了 写入2种数据 1种是订单数据 师傅订单生成 1种 记录当前这个师傅订单的状态
        // 抢单成功,签到,施工中,完成待结算,完成
        // 逆向订单 用户关闭 平台关闭 超时
        // 填补参数 userId orderNo status(抢单成功 10 20 30 40 50)
        Long userId = SecurityContext.getLoginToken().getUserId();
        String orderNo= UUID.randomUUID().toString();
        orderParam.setUserId(userId);
        orderParam.setOrderNo(orderNo);
        orderParam.setStatus(OrderStatusEnum.ORDER_CREATE_SUCCESS.getStatus());
        //insert into order
        orderRepository.save(orderParam);
        //insert into order_log
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(
                orderNo,
                userId,
                OrderStatusEnum.ORDER_CREATE_SUCCESS);
        orderLogRepository.save(orderLogParam);
        //3.发送一个延迟消息 消费一旦发现 未签到 cancel demand order
        eventService.sendDelaySignEvent(orderNo);
        return orderNo;
    }

    public ListRecordTotalBO<OrderBO> orderList(OrderQuery orderQuery) {
        //1. query条件组装完毕 看到前端传递数据pageNo pageSize status[] 缺少一个userId
        Long userId=SecurityContext.getLoginToken().getUserId();
        orderQuery.setUserId(userId);
        //2. 先查询total总条数
        Long total = orderRepository.getOrderCount(orderQuery);
        List<OrderBO> orderBOS=null;
        if (total>0){
            //2.1 total>0 在查询list列表数据
            orderBOS=orderRepository.queryOrders(orderQuery);
        }
        //2.2 total=0 包装一个null返回值
        return new ListRecordTotalBO<>(orderBOS,total);
    }
    //签到的订单一定存在么? 必须存在
    //如果存在就一定能签到么? 只有刚刚抢完单的订单状态 才可以修改成签到
    //UNDO 传递的参数应该伴随 师傅手机的定位信息,后端代码验证 定位数据是否符合签到条件500米范围.
    public String sign(OrderSignParam orderSignParam) throws BusinessException {
        String orderNo=orderSignParam.getOrderNo();
        //1.利用orderNo查询订单
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        //2.判断是否为空 为空抛异常
        if (orderBO==null||orderBO.getId()==null){
            throw new BusinessException(ResultEnum.ORDER_EMPTY);
        }
        //3.不为空,判断订单当前状态 必须是抢完成功 status=10 状态不正确 抛异常
        if(orderBO.getStatus()!=10){
            throw new BusinessException(ResultEnum.ORDER_STATUS_TRANSFORM_ERROR);
        }
        //4.订单order表格当前订单状态 status=20
        OrderParam orderParam=new OrderParam();
        orderParam.setOrderNo(orderNo);
        orderParam.setStatus(20);
        orderRepository.sign(orderParam);
        //5.记录order签到的日志操作
        Long userId=SecurityContext.getLoginToken().getUserId();
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(
                orderNo,
                userId,
                OrderStatusEnum.ORDER_SIGN_SUCCESS);
        orderLogRepository.save(orderLogParam);
        return orderNo;
    }
    @Autowired(required = false)
    private AttachApi attachApi;
    public String confirm(OrderConfirmParam orderConfirmParam) throws BusinessException {
        //1. 查询订单
        String orderNo=orderConfirmParam.getOrderNo();
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        //2. 判断存在
        if (orderBO==null||orderBO.getId()==null){
            throw new BusinessException(ResultEnum.ORDER_EMPTY);
        }
        //3. 状态是否流转正常 [20,50]
        if (orderBO.getStatus()!=20 && orderBO.getStatus()!=50){
            throw new BusinessException(ResultEnum.ORDER_STATUS_TRANSFORM_ERROR);
        }
        //4. 绑定图片 bizType=200 bizId=orderId TODO
        List<AttachUpdateParam> attaches=new ArrayList<>();
        for (Long attachId : orderConfirmParam.getAttachIds()) {
            Integer businessType=200;
            Integer businessId=orderBO.getId().intValue();
            Integer isCover=0;
            AttachUpdateParam attachUpdateParam=new AttachUpdateParam();
            attachUpdateParam.setId(attachId.intValue());
            attachUpdateParam.setIsCover(isCover);
            attachUpdateParam.setBusinessId(businessId);
            attachUpdateParam.setBusinessType(businessType);
            attaches.add(attachUpdateParam);
        }
        attachApi.batchUpdateAttachByIdList(attaches);
        //5. 更新订单状态 为50 条件是 status=20
        if (orderBO.getStatus()==20){
            //第一次批量绑定图片 更新订单状态 否则不需要更新
            OrderParam orderParam=new OrderParam();
            orderParam.setOrderNo(orderNo);
            orderParam.setStatus(50);
            orderRepository.confirm(orderParam);
        }
        //6. 每次绑定上传,都记录一次log
        Long userId=SecurityContext.getLoginToken().getUserId();
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(
                orderNo,
                userId,
                OrderStatusEnum.ORDER_ATTACH_CONFIRM);
        orderLogRepository.save(orderLogParam);
        return orderNo;
    }

    public OrderBO detail(String orderNo) throws BusinessException {
        //1. 使用orderNo查询orderBo
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        //2. 为空 抛异常
        if (orderBO==null||orderBO.getId()==null){
            throw new BusinessException(ResultEnum.ORDER_EMPTY);
        }
        //3. 获取图片信息 拼接成一个可以访问url
        List<AttachInfoBO> attaches=getAttachInfo(200,orderBO.getId().intValue());
        orderBO.setAttachInfoBO(attaches);
        //4. orderBO signTime签到时间 finishTime 完成待结算时间 需要orderLog获取
        //使用orderNo到order_log查询日志记录 多条记录信息,筛选2种 签到 服务完成待结算
        setTimes(orderBO);
        return orderBO;
    }

    private void setTimes(OrderBO orderBO) {
        //1. 使用orderBO中的orderNo查询orderLog记录 List 只需要2种 状态日志记录时间 sign签到 finish 完成待结算
        String orderNo=orderBO.getOrderNo();
        //TODO
        List<OrderLogBO> orderLogBOS = orderLogRepository.getOrderLogByOrderNo(orderNo);
        if (orderLogBOS == null || orderLogBOS.size()==1){
            //当前日志中没有签到和完成待结算的时间
            return;
        }
        //2. 筛选 不能使用status=20 status=30 只有字符串 签到和 完成待结算
        Long signTime=null;
        Long finishTime=null;
        for (OrderLogBO orderLogBO : orderLogBOS) {
            //operateName 等于 签到 赋值signTime 等于 服务完成待结算 赋值finishTime
            String operateName = orderLogBO.getOperateName();
            if ("签到".equals(operateName)){
                signTime=orderLogBO.getOperateTime();
            }else if("服务完成待结算".equals(operateName)){
                finishTime=orderLogBO.getOperateTime();
            }
        }
        //3. signTime finishTime set会当前orderBO
        orderBO.setSignTime(signTime);
        orderBO.setFinishTime(finishTime);
    }

    private List<AttachInfoBO> getAttachInfo(int bizType, int bizId) {
        AttachQuery attachQuery=new AttachQuery();
        attachQuery.setBusinessId(bizId);
        attachQuery.setBusinessType(bizType);
        List<AttachDTO> attachDTOS = attachApi.getAttachInfoByParam(attachQuery);
        //手动转化
        if (CollectionUtils.isEmpty(attachDTOS)){
            return null;
        }else {
            List<AttachInfoBO> attaches=new ArrayList<>();
            for (AttachDTO attachDTO : attachDTOS) {
                String url="http://localhost:8092/static/"+attachDTO.getFileUuid();
                AttachInfoBO attachInfoBO=new AttachInfoBO();
                attachInfoBO.setUrl(url);
                attaches.add(attachInfoBO);
            }
            return attaches;
        }
    }

    public String finish(OrderFinishParam orderFinishParam) throws BusinessException {
        String orderNo=orderFinishParam.getOrderNo();
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        //1. 使用orderNo校验订单是否存在 不存在抛异常
        if (orderBO==null||orderBO.getId()==null){
            throw new BusinessException(ResultEnum.ORDER_EMPTY);
        }
        //2. 状态流转 校验 不满足要求 抛异常 50-30 才正常
        if (orderBO.getStatus()!=50){
            throw new BusinessException(ResultEnum.ORDER_STATUS_TRANSFORM_ERROR);
        }
        //3. 更新订单状态
        OrderParam orderParam=new OrderParam();
        orderParam.setOrderNo(orderNo);
        orderParam.setStatus(30);
        //insert into order
        orderRepository.finish(orderParam);
        //4. 记录订单日志
        //insert into order_log
        Long userId=SecurityContext.getLoginToken().getUserId();
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(
                orderNo,
                userId,
                OrderStatusEnum.ORDER_SERVICE_SUCCESS_UN_SETTLE);
        orderLogRepository.save(orderLogParam);
        //5.发送
        OrderMqDTO orderMqDTO = orderAssembler.assembleOrderMqDTO(orderBO);
        eventService.sendOrderFinishEvent(orderMqDTO);
        return orderNo;
    }
    @Autowired(required = false)
    private EventService eventService;
    //考虑 消费业务幂等问题
    public void complete(String orderNo) {
        //1. 更新订单状态 必须是30才可以更新成40
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        //1. 使用orderNo校验订单是否存在 不存在抛异常
        if (orderBO==null||orderBO.getId()==null){
            //同样可以保证幂等 , 对于不存在的orderNo多次重复调用的限制
            log.info("当前完结的订单:{},不存在",orderNo);
            return;
        }
        if (orderBO.getStatus()!=30){
            //同样可以保证幂等 , 已经完成的 由于status=40 不会在继续调用代码
            log.info("当前完结订单:{}状态流转不正常,:{}",orderNo,orderBO.getStatus());
            return;
        }
        OrderParam orderParam=new OrderParam();
        orderParam.setOrderNo(orderNo);
        orderParam.setStatus(40);
        orderRepository.complete(orderParam);
        //2. 记录完结complete操作日志 不能在通过登录的jwt解析userId了 只能通过订单查询获取
        //或者 写死 平台完结的
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(
                orderNo,
                999L,
                OrderStatusEnum.ORDER_FINISH);
        orderLogRepository.save(orderLogParam);
    }
    @Transactional
    public void delay(String orderNo) {
        //1. 查询orderNo
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        //1.1 查不到 return
        if (orderBO.getId()==null){
            log.info("当前订单不存在:{}",orderNo);
            return;
        }
        //2. 判断状态 !=10 算正常 幂等保证
        if (orderBO.getStatus()!=10){
            //正常状态 订单已经被师傅 操作了 消费延迟归还需求单 不需要执行
            log.info("订单{}已经开始执行,无需延迟逻辑处理",orderNo);
            return;
        }
        //2.1 状态不正常 2小时未签到 远程调用还需求单
        //本地 修改订单 平台关闭订单 记录操作日志
        OrderParam orderParam=new OrderParam();
        orderParam.setOrderNo(orderNo);
        orderParam.setStatus(OrderStatusEnum.CANCELLED_BY_EXPIRE.getStatus());
        //update order set status=-30 where order_no=#{orderNo}
        orderRepository.finish(orderParam);
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(
                orderNo,
                999L,
                OrderStatusEnum.CANCELLED_BY_EXPIRE);
        orderLogRepository.save(orderLogParam);
        //dubbo调用 还需求单
        Boolean returned = requestOrderApi.returnOrder(orderBO.getRequestOrderNo());
        if (!returned){
            //怎么办呢?
            //保证一定归还 ,继续 补偿操作 比如发送一个消息.
        }
    }
}
