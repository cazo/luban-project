package com.tarena.tp.luban.order.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.order.po.OrderLog;
import com.tarena.tp.luban.order.server.dao.OrderLogDAO;
import com.tarena.tp.luban.order.server.domain.bo.OrderLogBO;
import com.tarena.tp.luban.order.server.domain.repository.OrderLogRepository;
import com.tarena.tp.luban.order.server.infrastructure.persistence.data.converter.OrderLogConverter;
import com.tarena.tp.luban.order.server.protocol.param.OrderLogParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderLogServerRepoImpl implements OrderLogRepository {
    @Autowired(required = false)
    private OrderLogDAO orderLogDAO;
    @Autowired
    private OrderLogConverter orderLogConverter;
    //订单状态每次变化 都增一条日志记录
    @Override
    public Long save(OrderLogParam orderLogParam) {
        //转化
        OrderLog orderLog = orderLogConverter.param2po(orderLogParam);
        //insert
        return orderLogDAO.insert(orderLog);
    }
    //删除某一个日志记录
    @Override
    public Integer delete(Long orderLogId) {
        return null;
    }
    //查询 某个订单的日志记录,展示 订单状态 详情
    @Override
    public List<OrderLogBO> getOrderLogByOrderNo(String orderNo) {
        //调用dao 查询 entity列表
        List<OrderLog> orderLogByOrderNo = orderLogDAO.getOrderLogByOrderNo(orderNo);
        //转化返回
        return orderLogConverter.poList2BoList(orderLogByOrderNo);
    }
    //查询已经完成的订单 师傅做筛选的时候
    @Override
    public List<OrderLogBO> selectFinishOrderLog(String orderNo) {
        return null;
    }
}
