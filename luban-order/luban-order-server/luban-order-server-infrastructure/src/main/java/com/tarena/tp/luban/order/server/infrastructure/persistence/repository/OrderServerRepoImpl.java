package com.tarena.tp.luban.order.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.order.po.Order;
import com.tarena.tp.luban.order.server.dao.OrderDAO;
import com.tarena.tp.luban.order.server.dao.query.OrderDBPagerQuery;
import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.repository.OrderRepository;
import com.tarena.tp.luban.order.server.infrastructure.persistence.data.converter.OrderConverter;
import com.tarena.tp.luban.order.server.protocol.param.OrderParam;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderServerRepoImpl implements OrderRepository {
    @Autowired(required = false)
    private OrderDAO orderDAO;
    @Autowired
    private OrderConverter orderConverter;
    //生成订单
    @Override
    public Long save(OrderParam orderParam) {
        //转化param2po
        Order order = orderConverter.param2po(orderParam);
        //insert
        return orderDAO.insert(order);
    }
    //删除订单
    @Override
    public Integer delete(Long orderId) {
        return null;
    }
    //使用订单id查询订单
    @Override
    public OrderBO getOrder(Long orderId) {
        return null;
    }
    //使用订单编号查询订单
    @Override
    public OrderBO getOrderByOrderNo(String orderNo) {
        //持久层查
        Order order = orderDAO.getOrderByOrderNo(orderNo);
        //转换类型返回
        return orderConverter.po2bo(order);
    }
    //抢到 修改状态操作
    @Override
    public void sign(OrderParam orderParam) {
        // 转化成po
        Order order = orderConverter.param2po(orderParam);
        // 应该在sql 拼接非空set条件
        orderDAO.updateStatus(order);
    }
    //施工确认 上传图片确认 绑定图片 修改状态
    @Override
    public void confirm(OrderParam orderParam) {
        // 转化成po
        Order order = orderConverter.param2po(orderParam);
        // 应该在sql 拼接非空set条件
        orderDAO.updateStatus(order);
    }
    //完成待结算
    @Override
    public int finish(OrderParam orderParam) {
        // 转化成po
        Order order = orderConverter.param2po(orderParam);
        // 应该在sql 拼接非空set条件
        return orderDAO.updateStatus(order);
    }
    //完成 打款结束
    @Override
    public void complete(OrderParam orderParam) {
        // 转化成po
        Order order = orderConverter.param2po(orderParam);
        // 应该在sql 拼接非空set条件
        orderDAO.updateStatus(order);
    }
    //分页查询订单列表
    @Override
    public Long getOrderCount(OrderQuery orderQuery) {
        //转化成为一个dbQuery
        OrderDBPagerQuery dbQuery = orderConverter.toDbPagerQuery(orderQuery);
        //读取dao 调用方法返回数据 SELECT COUNT(*) FROM ORDER WHERE USER_ID AND STATUS
        return orderDAO.countOrder(dbQuery);
    }

    //分页订单数据
    @Override
    public List<OrderBO> queryOrders(OrderQuery orderBOQuery) {
        //转化成为一个dbQuery
        OrderDBPagerQuery dbQuery = orderConverter.toDbPagerQuery(orderBOQuery);
        //读取dao 调用方法返回数据
        List<Order> orders = orderDAO.queryOrders(dbQuery);
        //将返回的entity列表转化成bo列表对象
        //SELECT * FROM ORDER WHERE USER_ID AND STATUS LIMTI OFFSET,ROWS
        return orderConverter.poList2BoList(orders);
    }
    //disable 师傅只能看到订单 但是无法操作订单
    @Override
    public Integer disable(String bankIds) {
        return null;
    }
    //enable
    @Override
    public Integer enable(String bankIds) {
        return null;
    }




}
