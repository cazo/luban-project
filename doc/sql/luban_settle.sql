/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.20 : Database - tarena_tp_luban_settle
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tarena_tp_luban_settle` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tarena_tp_luban_settle`;

/*Table structure for table `settle_bill` */

DROP TABLE IF EXISTS `settle_bill`;

CREATE TABLE `settle_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0' COMMENT '用户id',
  `type` tinyint(1) DEFAULT '0' COMMENT '状态 1:师傅，2:平台',
  `request_order_no` varchar(64) DEFAULT '' COMMENT '需求单',
  `worker_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '师傅id',
  `worker_name` varchar(64) NOT NULL DEFAULT '' COMMENT '师傅姓名',
  `worker_phone` varchar(64) NOT NULL DEFAULT '' COMMENT '师傅电话',
  `request_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '需求单客户id',
  `request_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '需求单客户姓名',
  `request_user_phone` varchar(64) NOT NULL DEFAULT '' COMMENT '需求单客户电话',
  `request_order_category_name` varchar(64) NOT NULL DEFAULT '' COMMENT '需求单类型',
  `request_order_price` int(10) unsigned DEFAULT '0' COMMENT '需求单价格',
  `order_no` varchar(64) DEFAULT '' COMMENT '交易订单号和提现订单号',
  `order_gmt_create` bigint(11) NOT NULL DEFAULT '0' COMMENT '订单创建时间',
  `total_amount` int(10) unsigned DEFAULT '0' COMMENT '总额',
  `scale` int(10) unsigned DEFAULT '0' COMMENT '比例',
  `amount` int(10) unsigned DEFAULT '0' COMMENT '实际收入',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态 0:无效，1:有效',
  `payment_time` bigint(11) DEFAULT '0' COMMENT '支付时间',
  `create_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `create_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `modified_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `modified_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `gmt_modified` bigint(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `gmt_create` bigint(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COMMENT='settle_bill';

/*Data for the table `settle_bill` */

insert  into `settle_bill`(`id`,`user_id`,`type`,`request_order_no`,`worker_id`,`worker_name`,`worker_phone`,`request_user_id`,`request_user_name`,`request_user_phone`,`request_order_category_name`,`request_order_price`,`order_no`,`order_gmt_create`,`total_amount`,`scale`,`amount`,`status`,`payment_time`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (17,999,2,'123123123',21,'代用名','13812353456',21,'王五','13012345678','厨具安装',100,'837120879920000011',1683712088009,20,20,20,2,1683712972008,'mock',99,99,'mock',1683712972008,1683712966663);
insert  into `settle_bill`(`id`,`user_id`,`type`,`request_order_no`,`worker_id`,`worker_name`,`worker_phone`,`request_user_id`,`request_user_name`,`request_user_phone`,`request_order_category_name`,`request_order_price`,`order_no`,`order_gmt_create`,`total_amount`,`scale`,`amount`,`status`,`payment_time`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (18,21,1,'123123123',21,'代用名','13812353456',21,'王五','13012345678','厨具安装',100,'837120879920000011',1683712088009,80,20,80,2,1683712972008,'mock',99,99,'mock',1683712972008,1683712966663);
insert  into `settle_bill`(`id`,`user_id`,`type`,`request_order_no`,`worker_id`,`worker_name`,`worker_phone`,`request_user_id`,`request_user_name`,`request_user_phone`,`request_order_category_name`,`request_order_price`,`order_no`,`order_gmt_create`,`total_amount`,`scale`,`amount`,`status`,`payment_time`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (19,999,2,'32232964103521',21,'代用名','13812353456',21,'张三','13266668888','衣柜安装',100,'837128358730010011',1683712835878,20,20,20,2,1683713565250,'mock',99,99,'mock',1683713565250,1683713560121);
insert  into `settle_bill`(`id`,`user_id`,`type`,`request_order_no`,`worker_id`,`worker_name`,`worker_phone`,`request_user_id`,`request_user_name`,`request_user_phone`,`request_order_category_name`,`request_order_price`,`order_no`,`order_gmt_create`,`total_amount`,`scale`,`amount`,`status`,`payment_time`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (20,21,1,'32232964103521',21,'代用名','13812353456',21,'张三','13266668888','衣柜安装',100,'837128358730010011',1683712835878,80,20,80,2,1683713565250,'mock',99,99,'mock',1683713565250,1683713560121);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
