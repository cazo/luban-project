/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.20 : Database - tarena_tp_luban_account
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tarena_tp_luban_account` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tarena_tp_luban_account`;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0' COMMENT '用户id',
  `user_name` varchar(16) DEFAULT '' COMMENT '用户名',
  `user_phone` varchar(16) DEFAULT '' COMMENT '用户手机',
  `settling_amount` int(10) unsigned DEFAULT '0' COMMENT '待结算总额',
  `total_amount` int(10) unsigned DEFAULT '0' COMMENT '结算金额',
  `create_user_name` varchar(16) DEFAULT '' COMMENT '创建人',
  `create_user_id` int(10) unsigned DEFAULT '0' COMMENT '创建人id',
  `modified_user_id` int(10) unsigned DEFAULT '0' COMMENT '修改人id',
  `modified_user_name` varchar(16) DEFAULT '' COMMENT '修改人',
  `gmt_create` bigint(20) DEFAULT '0' COMMENT '创建时间',
  `gmt_modified` bigint(20) DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态 0:无效，1:有效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='account';

/*Data for the table `account` */

insert  into `account`(`id`,`user_id`,`user_name`,`user_phone`,`settling_amount`,`total_amount`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (6,21,'代用名','13812353456',0,180,'',0,0,'mock',1683712088009,1683713565229,0);

/*Table structure for table `account_record` */

DROP TABLE IF EXISTS `account_record`;

CREATE TABLE `account_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0' COMMENT '用户id',
  `order_no` varchar(16) DEFAULT '' COMMENT '订单编号',
  `settlement_no` varchar(16) DEFAULT '' COMMENT '结算单编号',
  `total_amount` int(10) unsigned DEFAULT '0' COMMENT '修改人id',
  `amount` int(10) unsigned DEFAULT '0' COMMENT '入账金额',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态 0:无效，1:有效',
  `create_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `create_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `modified_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `modified_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `gmt_modified` bigint(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `gmt_create` bigint(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='account_record';

/*Data for the table `account_record` */

/*Table structure for table `bank_card` */

DROP TABLE IF EXISTS `bank_card`;

CREATE TABLE `bank_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0' COMMENT '用户id',
  `name` varchar(16) DEFAULT '' COMMENT '持卡人姓名',
  `bank_name` varchar(16) DEFAULT '' COMMENT '银行名称',
  `card_no` varchar(16) DEFAULT '' COMMENT '银行卡号',
  `is_default` tinyint(1) DEFAULT '0' COMMENT '状态 0:非默认，1:默认',
  `phone` varchar(16) DEFAULT '' COMMENT '手机号',
  `sort` int(10) unsigned DEFAULT '0' COMMENT '排序',
  `create_user_name` varchar(16) DEFAULT '' COMMENT '创建人',
  `create_user_id` int(10) unsigned DEFAULT '0' COMMENT '创建人id',
  `modified_user_id` int(10) unsigned DEFAULT '0' COMMENT '修改人id',
  `modified_user_name` varchar(16) DEFAULT '' COMMENT '修改人',
  `gmt_create` bigint(20) DEFAULT '0' COMMENT '创建时间',
  `gmt_modified` bigint(20) DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态 0:无效，1:有效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='bank_card';

/*Data for the table `bank_card` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
