package com.tarena.tp.luban.settle.server.web.consumer;

import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.settle.server.manager.SettleServerService;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic="order_settle_topic",consumerGroup = "settle_group")
public class SettleBillConsumer implements RocketMQListener<OrderMqDTO> {
    @Autowired
    private SettleServerService settleServerService;
    @Override
    public void onMessage(OrderMqDTO message) {
        //完成 settle业务方法
        //调用一下settle业务对象
        settleServerService.settle(message);
    }
}
