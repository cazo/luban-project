package com.tarena.tp.luban.settle.server.web.producer;

import com.tarena.tp.luban.settle.server.manager.EventService;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * 消息 目的 是让订单服务 对该订单进行完成操作
 * topic= order_complete_topic
 */
@Component
public class EventProducerImpl implements EventService {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Override
    public void sendSettledEvent(String orderNo) {
        //1. 整理封装消息
        Message<String> message = MessageBuilder.withPayload(orderNo).build();
        //2. 消息的发送
        rocketMQTemplate.asyncSend("order_complete_topic", message,
                new SendCallback() {
                    @Override
                    public void onSuccess(SendResult sendResult) {
                        System.out.println("成功");
                    }
                    @Override
                    public void onException(Throwable e) {
                        System.out.println("失败");
                    }
                });
    }
}
