package com.tarena.tp.luban.settle.server.manager;

import com.tarena.tp.luban.account.server.client.AccountApi;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.settle.server.assemble.SettleAssembler;
import com.tarena.tp.luban.settle.server.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.server.repository.SettleBillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SettleServerService {
    @Autowired(required = false)
    private SettleBillRepository settleBillRepository;
    @Autowired
    private SettleAssembler settleAssembler;
    @Autowired(required = false)
    private AccountApi accountApi;
    @Autowired(required = false)
    private EventService eventService;
    //业务代码 要考虑 幂等
    @Transactional
    public void settle(OrderMqDTO orderMqDTO) {
        //防止重复消费,方法幂等设计,使用订单编号 查询settleBill 如果存在 则订单已经结算
        Long size = settleBillRepository.getSettleBillByOrderNo(orderMqDTO.getOrderNo());
        if(size > 0){
            return;
        }
        //1. 根据消息数据生成 本地bill账单表格行 2行 一行记录师傅,一行记录平台
        // 数据类型的转化
        List<SettleBillParam> settleBillParams = settleAssembler.order2Params(orderMqDTO);
        settleBillRepository.batchSave(settleBillParams);
        //2. 远程调用模拟打款 dubbo rpc远程调用 只给师傅模拟打款
        PaymentParam pay=new PaymentParam();
        pay.setOrderNo(orderMqDTO.getOrderNo());
        pay.setUserId(orderMqDTO.getUserId());
        pay.setTotalAmount(settleBillParams.get(0).getTotalAmount());
        accountApi.mockPayment(pay);
        //3. 发送消息,订单服务消费完成订单. 消息 尽可能保证精简准确. 尤其是高并发消息
        eventService.sendSettledEvent(orderMqDTO.getOrderNo());
    }
}
