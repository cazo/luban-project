package com.tarena.tp.luban.settle.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.settle.po.SettleBill;
import com.tarena.tp.luban.settle.server.dao.SettleBillDAO;
import com.tarena.tp.luban.settle.server.infrastructure.persistence.converter.SettleBillConverter;
import com.tarena.tp.luban.settle.server.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.server.repository.SettleBillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SettleRepoImpl implements SettleBillRepository {
    @Autowired(required = false)
    private SettleBillDAO settleBillDAO;
    @Autowired
    private SettleBillConverter settleBillConverter;
    @Override
    public void batchSave(List<SettleBillParam> settleBillParams) {
        //转化
        List<SettleBill> settleBills = settleBillConverter.params2pos(settleBillParams);
        //批量写入
        settleBillDAO.batchInsert(settleBills);
    }
    //虽然是查询方法 实际返回的是count
    //select count(*)
    @Override
    public Long getSettleBillByOrderNo(String orderNo) {
        List<SettleBill> settleBills = settleBillDAO.getSettleBillByOrderNo(orderNo);
        if (settleBills!=null){
            return Long.parseLong(settleBills.size()+"");
        }
        return 0L;
    }
    @Override
    public void updateStatus(String orderNo, Integer status) {

    }


}
