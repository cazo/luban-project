/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.po;

import com.tedu.inn.protocol.dao.PO;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Table(name = "settle_bill")
public class SettleBill extends PO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(11)")
    /**
     * 主键
     */
    private Long id;
    /**
     * 用户id
     */
    @Column(name = "user_id", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '用户id'")
    private Long userId;
    /**
     * 结算类型
     */
    @Column(name = "type", columnDefinition = "tinyint(1)  DEFAULT 0 COMMENT '状态 1:师傅，2:平台'")
    private Integer type;

    /**
     * 需求单
     */
    @Column(name = "request_order_no", columnDefinition = "varchar(16) DEFAULT '' COMMENT '需求单'")
    private String requestOrderNo;


    /**
     * 需求单类型
     */
    @Column(name = "request_order_category_name", columnDefinition = "varchar(16) DEFAULT '' COMMENT '需求单类型'")
    private String requestOrderCategoryName;

    /**
     * 需求单类型
     */
    @Column(name = "request_order_price", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '需求单价格'")
    private Long requestOrderPrice;

    /**
     * 交易订单号
     */
    @Column(name = "order_no", columnDefinition = "varchar(16) DEFAULT '' COMMENT '交易订单号和提现订单号'")
    private String orderNo;

    /**
     * 总额
     */
    @Column(name = "total_amount", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '总额'")
    private Long totalAmount;

    /**
     * 比例
     */
    @Column(name = "scale", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '比例'")
    private Long scale;






    /**
     * 实际收入
     */
    @Column(name = "amount", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '实际收入'")
    private Long amount;

    /**
     * 是否有效状态
     */
    @Column(name = "status", columnDefinition = "tinyint(1)  DEFAULT 0 COMMENT '状态 0:无效，1:有效'")
    private Integer status;

    /**
     *  订单相关附属信息，方便后端查询
     */
    @Column(
            name = "worker_name",
            columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '师傅id'",
            nullable = false
    )
    private Long workerId;

    @Column(
            name = "worker_name",
            columnDefinition = "varchar(64)  DEFAULT '' COMMENT '师傅姓名'",
            nullable = false
    )
    private String workerName;

    @Column(
            name = "worker_phone",
            columnDefinition = "varchar(64)  DEFAULT '' COMMENT '师傅电话'",
            nullable = false
    )
    private String workerPhone;


    @Column(
            name = "request_user_id",
            columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '需求单用户id'",
            nullable = false
    )
    private Long requestUserId;

    @Column(
            name = "request_user_name",
            columnDefinition = "varchar(64)  DEFAULT '' COMMENT '需求单用户姓名'",
            nullable = false
    )
    private String requestUserName;

    @Column(
            name = "request_user_phone",
            columnDefinition = "varchar(64)  DEFAULT '' COMMENT '需求单用户电话'",
            nullable = false
    )
    private String requestUserPhone;


    @Column(
            name = "order_gmt_modified",
            columnDefinition = "bigint(11)  DEFAULT 0 COMMENT '订单创建时间'",
            nullable = false
    )
    private Long orderGmtCreate;



    @Column(
            name = "payment_time",
            columnDefinition = "bigint(11)  DEFAULT 0 COMMENT '入账时间'",
            nullable = false
    )
    private Long paymentTime;




}
