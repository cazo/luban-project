/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.admin.protocol.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SettleBillVO {

 @ApiModelProperty("订单编号")
 String orderNo;

 @ApiModelProperty("订单成交时间")
 Long orderGmtCreate;

 @ApiModelProperty("入账时间")
 Long paymentTime;


 @ApiModelProperty("订单类型")
 String requestOrderCategoryName;

 @ApiModelProperty("订单金额")
 Long requestOrderPrice;

 @ApiModelProperty("用户姓名")
 String requestUserName;

 @ApiModelProperty("用户手机号")
 String requestUserPhone;

 @ApiModelProperty("师傅姓名")
 String workerName;

 @ApiModelProperty("师傅手机号")
 String workerPhone;

 @ApiModelProperty("分润比例")
 Long scale;

 @ApiModelProperty("收入")
 Long amount;

 @ApiModelProperty("分润比例 1:结算中 2:已入账")
 Integer status;

}