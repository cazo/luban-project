/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.admin.assemble;

import com.tarena.tp.luban.settle.admin.bo.SettleBillBO;
import com.tarena.tp.luban.settle.admin.protocol.vo.SettleBillVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.pager.PagerResult;
import com.tedu.inn.protocol.pager.SimplePagerQuery;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class SettleBillAssemble {
    public SettleBillVO assembleBO2VO(SettleBillBO bo) {
        SettleBillVO settleBill = new SettleBillVO();
        BeanUtils.copyProperties(bo, settleBill);
        return settleBill;
    }

    public List<SettleBillVO> boListAssembleVOList(List<SettleBillBO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }
        List<SettleBillVO> settleBillVOList = new ArrayList<>(list.size());
        for (SettleBillBO settleBillBo : list) {
            SettleBillVO settleBillVo = this.assembleBO2VO(settleBillBo);
            settleBillVOList.add(settleBillVo);
        }
        return settleBillVOList;
    }

    public PagerResult<SettleBillVO> assemblePagerResult(ListRecordTotalBO<SettleBillBO> settleBillListTotalRecord,
        SimplePagerQuery settleBillQuery) {
        List<SettleBillVO> settleBillVOList = this.boListAssembleVOList(settleBillListTotalRecord.getList());
        PagerResult<SettleBillVO> pagerResult = new PagerResult<>(settleBillQuery);
        pagerResult.setObjects(settleBillVOList);
        pagerResult.setTotal(settleBillListTotalRecord.getTotal());
        return pagerResult;
    }
}