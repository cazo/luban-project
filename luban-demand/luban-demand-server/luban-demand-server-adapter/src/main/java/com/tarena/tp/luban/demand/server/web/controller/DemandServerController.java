package com.tarena.tp.luban.demand.server.web.controller;

import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.manager.DemandServerService;
import com.tarena.tp.luban.demand.server.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.server.web.assemble.RequestOrderAssemble;
import com.tarena.tp.luban.demand.server.web.vo.RequestOrderListItemVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.pager.PagerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemandServerController {
    /**
     * @param requestOrderQuery 服务区域ids 服务品类ids 前端根据师傅详情 拼接的数据
     * TODO 如果前端没有传递服务区域和种类 ,可以dubbo worker-server 检查当前登录的userId绑定
     *      师傅信息补充参数
     * @return
     */
    @Autowired
    private DemandServerService demandServerService;
    @Autowired
    private RequestOrderAssemble requestOrderAssemble;
    @PostMapping("/demand/order/search")
    public PagerResult<RequestOrderListItemVO> demandList(
            @RequestBody RequestOrderQuery requestOrderQuery){
        //TODO 返回业务查询数据 boList
        ListRecordTotalBO<RequestOrderBO> demandBOS=
                demandServerService.demandList(requestOrderQuery);
        //转化成 vo 分页返回
        return requestOrderAssemble.assemblePagerResult(demandBOS,requestOrderQuery);
    }
}
