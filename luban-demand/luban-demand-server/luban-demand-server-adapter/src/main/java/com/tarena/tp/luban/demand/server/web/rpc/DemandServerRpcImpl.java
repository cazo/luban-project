package com.tarena.tp.luban.demand.server.web.rpc;

import com.tarena.tp.luban.demand.server.client.RequestOrderApi;
import com.tarena.tp.luban.demand.server.manager.DemandServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DemandServerRpcImpl implements RequestOrderApi {
    @Autowired
    private DemandServerService demandServerService;
    @Override
    public Boolean grabOrder(String requestOrderNo) {
        return demandServerService.grabOrder(requestOrderNo);
    }

    @Override
    public Boolean returnOrder(String requestOrderNo) {
        return demandServerService.returnOrder(requestOrderNo);
    }
}
