package com.tarena.tp.luban.demand.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.demand.po.RequestOrder;
import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.dao.RequestOrderDao;
import com.tarena.tp.luban.demand.server.dao.param.GrabOrderParam;
import com.tarena.tp.luban.demand.server.dao.query.RequestOrderDBPagerQuery;
import com.tarena.tp.luban.demand.server.infrastructure.persistence.data.converter.RequestOrderConverter;
import com.tarena.tp.luban.demand.server.protocol.param.RequestOrderParam;
import com.tarena.tp.luban.demand.server.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.server.repository.RequestOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DemandServerRepoImpl implements RequestOrderRepository {
    @Autowired(required = false)
    private RequestOrderDao requestOrderDao;
    @Autowired
    private RequestOrderConverter requestOrderConverter;
    @Override
    public Long getRequestOrderCount(RequestOrderQuery requestOrderQuery) {
        //参数转化为dbQuery
        RequestOrderDBPagerQuery dbQuery =
                requestOrderConverter.toDbPagerQuery(requestOrderQuery);
        //dao 持久层 查询
        return requestOrderDao.countRequestOrder(dbQuery);
    }

    @Override
    public Integer returnOrder(String requestOrderNo) {

            GrabOrderParam grabOrderParam=new GrabOrderParam();
            grabOrderParam.setRequestOrderNO(requestOrderNo);
            Integer result=requestOrderDao.returnOrder(grabOrderParam);
            return result;
    }

    @Override
    public List<RequestOrderBO> queryRequestOrders(RequestOrderQuery requestOrderQuery) {
        //条件转化成dbQuery
        RequestOrderDBPagerQuery dbQuery =
                requestOrderConverter.toDbPagerQuery(requestOrderQuery);
        //dao 查询
        List<RequestOrder> requestOrders = requestOrderDao.queryRequestOrders(dbQuery);
        //结果转化bo
        return requestOrderConverter.poList2BoList(requestOrders);
    }


    @Override
    public Long save(RequestOrderParam requestOrderParam) {
        return null;
    }

    @Override
    public Integer grabOrder(String requestOrderNo) {
        //查询需求单对象 select * from request_order where request_order_no and grab_status
        RequestOrder requestOrder = requestOrderDao.getRequestOrderByOrderNo(requestOrderNo);
        if (requestOrder!=null){
            //判断对象存在 获取version
            Integer version = requestOrder.getVersion();
            GrabOrderParam grabOrderParam=new GrabOrderParam();
            grabOrderParam.setVersion(version);
            grabOrderParam.setRequestOrderNO(requestOrderNo);
            Integer result=requestOrderDao.grabOrderCas(grabOrderParam);
            return result;
        }else {
            //不存在可抢的需求单直接返回0
            return 0;
        }
    }

    @Override
    public Integer cancelGrabOrder(String requestOrderNo) {
        return null;
    }

    @Override
    public RequestOrderBO getRequestOrder(String requestOrderNo) {
        return null;
    }


}
