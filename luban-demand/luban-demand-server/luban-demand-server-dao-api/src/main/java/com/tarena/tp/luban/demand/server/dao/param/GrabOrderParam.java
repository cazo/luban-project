package com.tarena.tp.luban.demand.server.dao.param;

import lombok.Data;

@Data
public class GrabOrderParam {
    private String requestOrderNO;
    private Integer version;
}
