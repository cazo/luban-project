package com.tarena.tp.luban.demand.server.manager;

import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.server.repository.RequestOrderRepository;
import com.tedu.inn.protocol.ListRecordTotalBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class DemandServerService {
    @Autowired(required = false)
    private RequestOrderRepository requestOrderRepository;
    public ListRecordTotalBO<RequestOrderBO> demandList(RequestOrderQuery requestOrderQuery) {
        //TODO query中服务品类和服务区域做校验和封装
        //查询所有数据total总数 TODO
        //查询所有可以抢单的需求单. grab_status 0 没抢 1 已抢
        Long total=requestOrderRepository.getRequestOrderCount(requestOrderQuery);
        //准备一个返回数据
        List<RequestOrderBO> requestOrderBOS=null;
        if (total>0){
            //只有total总数大于0 才继续查询 TODO
            requestOrderBOS=requestOrderRepository.queryRequestOrders(requestOrderQuery);
            //元素对象缺少属性 viewOrderAmount profitScale(来自于厂商的信息 1-20%)
            for (RequestOrderBO requestOrderBO : requestOrderBOS) {
                replenishRequestOrderBO(requestOrderBO);
            }
        }
        return new ListRecordTotalBO<>(requestOrderBOS,total);
    }

    private void replenishRequestOrderBO(RequestOrderBO requestOrderBO) {
        //定义profitScale 20
        requestOrderBO.setProfitScale(new BigDecimal(20));
        //对象携带的总价钱
        Long orderAmount = requestOrderBO.getOrderAmount();
        // (100-profitScale)/100=80% 师傅分润 80%* orderAmmount
        BigDecimal workerPercent=
                new BigDecimal(100)
                        .subtract(requestOrderBO.getProfitScale())
                        .divide(new BigDecimal(100));
        Long viewOrderAmount=
                new BigDecimal(orderAmount).multiply(workerPercent).longValue();
        requestOrderBO.setViewOrderAmount(viewOrderAmount);
    }

    public Boolean grabOrder(String requestOrderNo) {
        Integer result = requestOrderRepository.grabOrder(requestOrderNo);
        //1 表示抢到 0 表示没抢到
        return result==1;
    }

    public Boolean returnOrder(String requestOrderNo) {
        //调用仓储层还
        Integer result =requestOrderRepository.returnOrder(requestOrderNo);
        return result==1;
    }
}
