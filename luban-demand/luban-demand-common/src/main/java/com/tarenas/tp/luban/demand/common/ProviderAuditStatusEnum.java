/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tarenas.tp.luban.demand.common;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

/**
 * 订单状态枚举
 */
@Getter
public enum ProviderAuditStatusEnum {


    PROVIDER_AUDIT_STATUS_WAIT(0, "未审核"),

    PROVIDER_AUDIT_STATUS_SUCCESS(1, "审核通过"),

    PROVIDER_AUDIT_STATUS_REJECT(2, "驳回"),

    ;

    ProviderAuditStatusEnum(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

    private Integer status;

    private String message;

    public static List<Integer> getUnAudit(){
        return Arrays.asList(PROVIDER_AUDIT_STATUS_WAIT.status, PROVIDER_AUDIT_STATUS_REJECT.status);
    }

}
