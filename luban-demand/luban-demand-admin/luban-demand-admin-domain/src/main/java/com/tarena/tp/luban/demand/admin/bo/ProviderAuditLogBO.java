package com.tarena.tp.luban.demand.admin.bo;

import lombok.Data;

@Data
public class ProviderAuditLogBO {

    private String userName;
    private Long operateTime;
    private Integer auditStatus;
    private String rejectReason;
    private String remark;

}
