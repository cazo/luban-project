/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.admin.infrastructure.persistence.data.converter;

import com.tarena.tp.luban.demand.admin.protocol.param.ProfitScaleParam;
import com.tarenas.tp.luban.demand.common.ProviderAuditStatusEnum;
import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.demand.admin.bo.AccessProviderBO;
import com.tarena.tp.luban.demand.po.AccessProvider;
import com.tarena.tp.luban.demand.admin.protocol.param.AccessProviderParam;
import com.tarena.tp.luban.demand.admin.protocol.query.AccessProviderQuery;
import com.tarena.tp.luban.demand.admin.dao.query.AccessProviderDBPagerQuery;
import com.tedu.inn.protocol.enums.StatusRecord;
import org.springframework.beans.BeanUtils;
import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class AccessProviderConverter{

   public AccessProviderDBPagerQuery toDbPagerQuery(AccessProviderQuery accessProviderQuery) {
           if (accessProviderQuery == null) {
               return new AccessProviderDBPagerQuery();
           }
           AccessProviderDBPagerQuery accessProviderDBPagerQuery = new AccessProviderDBPagerQuery();
           BeanUtils.copyProperties(accessProviderQuery, accessProviderDBPagerQuery);
           return accessProviderDBPagerQuery;
       }
   

     public AccessProvider param2po(AccessProviderParam param) {
        AccessProvider accessProvider = new AccessProvider();
        BeanUtils.copyProperties(param, accessProvider);

        LoginUser loginUser = SecurityContext.getLoginToken();

        accessProvider.setGmtCreate(System.currentTimeMillis());
        accessProvider.setGmtModified(accessProvider.getGmtCreate());
        accessProvider.setCreateUserId(loginUser.getUserId());
        accessProvider.setModifiedUserId(loginUser.getUserId());
        if (param.getAuditStatus() != null) {
            accessProvider.setAuditStatus(param.getAuditStatus());
        }else {
            accessProvider.setAuditStatus(ProviderAuditStatusEnum.PROVIDER_AUDIT_STATUS_WAIT.getStatus());
        }
        accessProvider.setStatus(StatusRecord.ENABLE.getStatus());
        accessProvider.setCreateUserName(loginUser.getUserName());
        accessProvider.setModifiedUserName(loginUser.getUserName());
        return accessProvider;
    }

    public AccessProvider profitScaleParam2po(ProfitScaleParam param){
        AccessProvider accessProvider = new AccessProvider();
        BeanUtils.copyProperties(param, accessProvider);
        return accessProvider;
    }



     public AccessProviderBO po2bo(AccessProvider accessProvider) {
        AccessProviderBO accessProviderBO = new AccessProviderBO();
        BeanUtils.copyProperties(accessProvider, accessProviderBO);
        return accessProviderBO;
    }

     public List<AccessProviderBO> poList2BoList(List<AccessProvider> list) {
        List<AccessProviderBO> accessProviderBos = new ArrayList<>(list.size());
        for (AccessProvider accessProvider : list) {
            accessProviderBos.add(this.po2bo(accessProvider));
        }
        return accessProviderBos;
    }

    public void convertStatus(StatusCriteria statusCriteria){
            LoginUser loginUser = SecurityContext.getLoginToken();
            statusCriteria.setModifiedUserId(loginUser.getUserId());
            statusCriteria.setModifiedUserName(loginUser.getUserName());
            statusCriteria.setGmtModified(System.currentTimeMillis());
    }
}