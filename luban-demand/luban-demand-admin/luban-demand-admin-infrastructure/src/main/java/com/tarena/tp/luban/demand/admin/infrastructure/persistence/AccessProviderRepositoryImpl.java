/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.admin.infrastructure.persistence;

import com.tarena.tp.luban.demand.admin.dao.AccessProviderDAO;
import com.tarena.tp.luban.demand.admin.protocol.param.ProfitScaleParam;
import com.tedu.inn.protocol.enums.StatusRecord;
import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.demand.admin.infrastructure.persistence.data.converter.AccessProviderConverter;
import com.tarena.tp.luban.demand.po.AccessProvider;
import com.tarena.tp.luban.demand.admin.bo.AccessProviderBO;
import com.tarena.tp.luban.demand.admin.protocol.param.AccessProviderParam;
import com.tarena.tp.luban.demand.admin.repository.AccessProviderRepository;
import com.tarena.tp.luban.demand.admin.protocol.query.AccessProviderQuery;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccessProviderRepositoryImpl implements AccessProviderRepository {
    @Autowired
    private AccessProviderConverter accessProviderConverter;

    @Autowired
    private AccessProviderDAO accessProviderDao;

    @Override public Long save(AccessProviderParam accessProviderParam) {
        AccessProvider accessProvider = this.accessProviderConverter.param2po(accessProviderParam);
        if (accessProvider.getId() != null) {
            this.accessProviderDao.update(accessProvider);
            return accessProvider.getId();
        }
        this.accessProviderDao.insert(accessProvider);
        return  accessProvider.getId();
    }

    @Override public Integer disable(String accessProviderId) {
        StatusCriteria statusCriteria = new StatusCriteria(accessProviderId, StatusRecord.DISABLE.getStatus());
        this.accessProviderConverter.convertStatus(statusCriteria);
        return this.accessProviderDao.changeStatus(statusCriteria);
    }

    @Override public Integer enable(String accessProviderId) {
        StatusCriteria statusCriteria = new StatusCriteria(accessProviderId, StatusRecord.ENABLE.getStatus());
        this.accessProviderConverter.convertStatus(statusCriteria);
        return this.accessProviderDao.changeStatus(statusCriteria);
    }

    @Override public AccessProviderBO getAccessProvider(Long accessProviderId) {
        AccessProvider accessProvider = this.accessProviderDao.getEntity(accessProviderId);
        return this.accessProviderConverter.po2bo(accessProvider);
    }

    @Override
    public Integer updateProfitScale(ProfitScaleParam profitScaleParam) {
        AccessProvider accessProvider = this.accessProviderConverter.profitScaleParam2po(profitScaleParam);
        return this.accessProviderDao.update(accessProvider);
    }

    @Override public List<AccessProviderBO> queryAccessProviders(
        AccessProviderQuery accessProviderQuery) {
        List<AccessProvider> accessProviderList = this.accessProviderDao.queryAccessProviders(this.accessProviderConverter.toDbPagerQuery(accessProviderQuery));
        return this.accessProviderConverter.poList2BoList(accessProviderList);
    }

    @Override public Long getAccessProviderCount(AccessProviderQuery accessProviderQuery) {
        return this.accessProviderDao.countAccessProvider(this.accessProviderConverter.toDbPagerQuery(accessProviderQuery));
    }
}