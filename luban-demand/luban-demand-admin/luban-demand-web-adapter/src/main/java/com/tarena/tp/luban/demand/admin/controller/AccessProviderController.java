/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.admin.controller;

import com.tarena.tp.luban.demand.admin.assemble.AccessProviderAssemble;
import com.tarena.tp.luban.demand.admin.bo.AccessProviderBO;
import com.tarena.tp.luban.demand.admin.manager.AccessProviderService;
import com.tarena.tp.luban.demand.admin.protocol.param.AccessProviderParam;
import com.tarena.tp.luban.demand.admin.protocol.param.ProfitScaleParam;
import com.tarena.tp.luban.demand.admin.protocol.param.ProviderAuditLogParam;
import com.tarena.tp.luban.demand.admin.protocol.query.AccessProviderQuery;
import com.tarena.tp.luban.demand.admin.protocol.vo.AccessProviderListItemVO;
import com.tarena.tp.luban.demand.admin.protocol.vo.AccessProviderVO;
import com.tarenas.tp.luban.demand.common.ProviderAuditStatusEnum;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.pager.PagerResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;

@Api(value = "accessProvider",tags = "供应商接口")
@RestController
@RequestMapping("admin/demand/provider")
public class AccessProviderController {

    @Autowired
    private AccessProviderService accessProviderService;

    @Autowired
    private AccessProviderAssemble accessProviderAssemble;

    @PostMapping(value = "search")
    @ResponseBody
    @ApiOperation("待审核列表")
    public PagerResult<AccessProviderListItemVO> queryAccessProviders(@RequestBody AccessProviderQuery accessProviderQuery) {
        accessProviderQuery.setAuditStatus(ProviderAuditStatusEnum.getUnAudit());
        ListRecordTotalBO<AccessProviderBO> accessProviderListTotalRecord = this.accessProviderService.queryAccessProvider(accessProviderQuery);
        return this.accessProviderAssemble.assemblePagerResult(accessProviderListTotalRecord, accessProviderQuery);
    }

    @PostMapping(value = "audited/search")
    @ResponseBody
    @ApiOperation("审核通过列表")
    public PagerResult<AccessProviderListItemVO> queryAuditedAccessProviders(@RequestBody AccessProviderQuery accessProviderQuery) {
        accessProviderQuery.setAuditStatus(Collections.singletonList(ProviderAuditStatusEnum.PROVIDER_AUDIT_STATUS_SUCCESS.getStatus()));
        ListRecordTotalBO<AccessProviderBO> accessProviderListTotalRecord = this.accessProviderService.queryAccessProvider(accessProviderQuery);
        return this.accessProviderAssemble.assemblePagerResult(accessProviderListTotalRecord, accessProviderQuery);
    }

    @PostMapping(value="save")
    @ResponseBody
    @ApiOperation("创建")
    public Long saveAccessProvider(@RequestBody AccessProviderParam accessProviderParam) throws BusinessException {
        return this.accessProviderService.saveAccessProvider(accessProviderParam);
    }

    @GetMapping(value="info")
    @ApiOperation("详情")
    public AccessProviderVO getAccessProvider(@RequestParam Long accessProviderId) throws BusinessException {
        AccessProviderBO accessProviderBo = accessProviderService.getAccessProvider(accessProviderId);
        return this.accessProviderAssemble.assembleBO2AccessProviderVO(accessProviderBo);
    }

    @PostMapping("modify/scale")
    @ApiOperation("调整分润比例")
    public Integer updateProfitScale(@RequestBody ProfitScaleParam profitScaleParam) throws BusinessException {
        return this.accessProviderService.updateProfitScale(profitScaleParam);
    }

    @PostMapping("enable")
    @ApiOperation("启用")
    public Integer enableAccessProvider(@RequestParam String accessProviderId) throws BusinessException {
        return this.accessProviderService.enableAccessProvider(accessProviderId);
    }

    @PostMapping("disable")
    @ApiOperation("禁用")
    public Integer disableAccessProvider(@RequestParam String accessProviderId) throws BusinessException {
        return this.accessProviderService.disableAccessProvider(accessProviderId);
    }


    @GetMapping(value="audit/info")
    @ApiOperation("审核详情页")
    public AccessProviderVO processProviderVO(@RequestParam Long accessProviderId) throws BusinessException {
        AccessProviderBO processProviderBO = accessProviderService.processProviderVO(accessProviderId);
        return this.accessProviderAssemble.assembleBO2AccessProviderVO(processProviderBO);
    }


    @PostMapping("audit")
    @ApiOperation("审核供应商")
    public Integer processProvider(@RequestBody ProviderAuditLogParam providerAuditLogParam){
        return this.accessProviderService.processProvider(providerAuditLogParam);
    }


}