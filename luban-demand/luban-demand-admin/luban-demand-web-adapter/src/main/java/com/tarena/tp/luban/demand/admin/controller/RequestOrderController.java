/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tarena.tp.luban.demand.admin.controller;


import com.tarena.tp.luban.demand.admin.assemble.RequestOrderAssemble;
import com.tarena.tp.luban.demand.admin.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.admin.manager.RequestOrderService;
import com.tarena.tp.luban.demand.admin.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.admin.protocol.vo.RequestOrderListItemVO;
import com.tarena.tp.luban.demand.admin.protocol.vo.RequestOrderVO;
import com.tarenas.tp.luban.demand.common.OrderStatusStatusEnum;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.pager.PagerResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "requestOrder",tags = "需求订单接口")
@RestController
@RequestMapping("admin/demand/order")
public class RequestOrderController {

    @Autowired
    private RequestOrderService requestOrderService;

    @Autowired
    private RequestOrderAssemble requestOrderAssemble;

    @PostMapping("search")
    @ResponseBody
    @ApiOperation("未抢列表")
    public PagerResult<RequestOrderListItemVO> queryDemandOrder(@RequestBody RequestOrderQuery requestOrderQuery) {
        requestOrderQuery.setGrabStatus(OrderStatusStatusEnum.ORDER_STATUS_WAIT_GRAB.getStatus());
        ListRecordTotalBO<RequestOrderBO> accessProviderListTotalRecord = this.requestOrderService.queryRequestOrder(requestOrderQuery);
        return this.requestOrderAssemble.assemblePagerResult(accessProviderListTotalRecord, requestOrderQuery);
    }

    @GetMapping(value="info")
    @ApiOperation("详情")
    public RequestOrderVO getRequestOrder(@RequestParam Long requestOrderId) throws BusinessException {
        RequestOrderBO requestOrderBO = requestOrderService.getRequestOrder(requestOrderId);
        return this.requestOrderAssemble.assembleBO2RequestOrderVO(requestOrderBO);
    }


}
