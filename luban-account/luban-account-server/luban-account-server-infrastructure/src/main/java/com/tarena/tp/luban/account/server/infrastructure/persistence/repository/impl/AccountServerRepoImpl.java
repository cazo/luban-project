package com.tarena.tp.luban.account.server.infrastructure.persistence.repository.impl;

import com.tarena.tp.luban.account.bo.AccountBO;
import com.tarena.tp.luban.account.po.Account;
import com.tarena.tp.luban.account.server.dao.AccountDAO;
import com.tarena.tp.luban.account.server.infrastructure.persistence.data.converter.AccountConverter;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.account.server.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AccountServerRepoImpl implements AccountRepository {
    @Autowired(required = false)
    private AccountDAO accountDAO;
    @Autowired
    private AccountConverter accountConverter;
    @Override
    public Long create(AccountParam accountCreateParam) {
        //转化 param2po
        Account account = accountConverter.param2po(accountCreateParam);
        //dao insert
        accountDAO.insert(account);
        return account.getId();
    }

    @Override
    public AccountBO getAccountByUserId(Long userId) {
        //dao查询
        Account account = accountDAO.getAccountByUserId(userId);
        //转化bo返回
        return accountConverter.po2bo(account);
    }

    @Override
    public Long updateAmount(PaymentParam param) {
        //转化
        Account account = accountConverter.param2po(param);
        //调用update 返回影响的行数
        return accountDAO.updateAmount(account);
    }
}
