package com.tarena.tp.luban.account.server.rpc;

import com.tarena.tp.luban.account.bo.AccountBO;
import com.tarena.tp.luban.account.server.assemble.AccountAssemble;
import com.tarena.tp.luban.account.server.client.AccountApi;
import com.tarena.tp.luban.account.server.dto.AccountDTO;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.account.server.service.AccountServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 对外暴露accountApi实现,接收所有的rpc请求
 */
@Component(value = "accountApiImpl")
public class AccountRpcImpl implements AccountApi {
    //注入业务层
    @Autowired
    private AccountServerService accountServerService;
    @Autowired
    private AccountAssemble accountAssemble;
    @Override
    public Long create(AccountParam accountCreateParam) {
        return accountServerService.create(accountCreateParam);
    }
    @Override
    public AccountDTO getAccountByUserId(Long userId) {
        //业务层调用获取 返回结果 BO
        AccountBO accountBO=accountServerService.getAccount(userId);
        //转化bo成dto
        return accountAssemble.assembleAccountDTO(accountBO);
    }
    @Override
    public Long mockPayment(PaymentParam param) {
        // update account set amount=amount+#{amount} where user_id=#{userId}
        return accountServerService.updateAmount(param);
    }
}
