package com.tarena.tp.luban.account.server.service;

import com.tarena.tp.luban.account.bo.AccountBO;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.account.server.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountServerService {
    @Autowired(required = false)
    private AccountRepository accountRepository;
    //需要不需要设计成幂等 同一个userId 创建账号 多次提交
    public Long create(AccountParam accountCreateParam) {
        //查询 是否存在userId绑定的账号 ,如果有 返回-1 如果没有创建返回 account账户id
        //由于 并发不会出现太高 因为是同一个userId,不需要引入分布式锁解决这种线程安全问题
        AccountBO accountBO = accountRepository.getAccountByUserId(accountCreateParam.getUserId());
        if (accountBO == null || accountBO.getTotalAmount() == null){
            //账户不存在
            return accountRepository.create(accountCreateParam);
        }
        return -1l;//已存在绑定用户的账号
    }

    public AccountBO getAccount(Long userId) {
        return accountRepository.getAccountByUserId(userId);
    }

    public Long updateAmount(PaymentParam param) {
        return accountRepository.updateAmount(param);
    }
}
