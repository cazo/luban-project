/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.repository;
import com.tarena.tp.luban.account.admin.bo.AccountRecordBO;
import com.tarena.tp.luban.account.admin.protocol.param.AccountRecordParam;
import com.tarena.tp.luban.account.admin.protocol.query.AccountRecordQuery;
import java.util.List;

public interface AccountRecordRepository {
    Long save(AccountRecordParam accountRecordParam);

    Integer delete(Long accountRecordId);

    AccountRecordBO getAccountRecord(Long accountRecordId);

    Long getAccountRecordCount(AccountRecordQuery accountRecordQuery);

    List<AccountRecordBO> queryAccountRecords(AccountRecordQuery accountRecordBOQuery);

    Integer disable(String bankIds);

    Integer enable(String bankIds);
}