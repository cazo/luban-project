/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.manager;

import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tarena.tp.luban.account.admin.bo.AccountRecordBO;
import com.tarena.tp.luban.account.admin.repository.AccountRecordRepository;
import com.tarena.tp.luban.account.admin.protocol.param.AccountRecordParam;
import com.tarena.tp.luban.account.admin.protocol.query.AccountRecordQuery;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class AccountRecordService {
    @Autowired
    private AccountRecordRepository accountRecordRepository;

    private void validateSaveAccountRecord(
        AccountRecordParam accountRecordParam) throws BusinessException {
        //Asserts.isTrue(StringUtility.isNullOrEmpty(accountRecordParam.getName()), SecurityAdminError.NAME_IS_EMPTY, AccountRecordSuffix.name);
    }

    public Long saveAccountRecord(
        AccountRecordParam accountRecordParam) throws BusinessException {
        this.validateSaveAccountRecord(accountRecordParam);
        return this.accountRecordRepository.save(accountRecordParam);
    }

    public Integer deleteAccountRecord(Long accountRecordId) throws BusinessException {
        //Asserts.isTrue(StringUtility.isNullOrEmpty(accountRecordId), SecurityAdminError.AccountRecord_ID_IS_EMPTY);
        return this.accountRecordRepository.delete(accountRecordId);
    }

    public Integer enableAccountRecord(String accountRecordIds) throws BusinessException {
        //Asserts.isTrue(StringUtils.isEmpty(bankIds), AccountRecordAdminError.ID_IS_EMPTY);
        return this.accountRecordRepository.enable(accountRecordIds);
    }

    public Integer disableAccountRecord(String accountRecordIds) throws BusinessException {
        //Asserts.isTrue(StringUtils.isEmpty(accountRecordIds), AccountRecordAdminError.ID_IS_EMPTY);
        return this.accountRecordRepository.disable(accountRecordIds);
    }

    public ListRecordTotalBO<AccountRecordBO> queryAllAccountRecord() {
        return queryAccountRecord(null);
    }

    public ListRecordTotalBO<AccountRecordBO> queryAccountRecord(
        AccountRecordQuery accountRecordQuery) {
        Long totalRecord = this.accountRecordRepository.getAccountRecordCount(accountRecordQuery);
        List<AccountRecordBO> accountRecordBoList = null;
        if (totalRecord > 0) {
            accountRecordBoList = this.accountRecordRepository.queryAccountRecords(accountRecordQuery);
        }
        return new ListRecordTotalBO<>(accountRecordBoList, totalRecord);
    }

    public AccountRecordBO getAccountRecord(
        Long accountRecordId) throws BusinessException {
        //Asserts.isTrue(accountRecordId == null, AccountRecordAdminError.IS_EMPTY);
        return this.accountRecordRepository.getAccountRecord(accountRecordId);
    }
}