/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.controller;

import com.tarena.tp.luban.account.admin.assemble.AccountAssemble;
import com.tarena.tp.luban.account.admin.bo.AccountBO;
import com.tarena.tp.luban.account.admin.manager.AccountService;
import com.tarena.tp.luban.account.admin.protocol.param.AccountParam;
import com.tarena.tp.luban.account.admin.protocol.query.AccountQuery;
import com.tarena.tp.luban.account.admin.protocol.vo.AccountVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.pager.PagerResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "account",tags = "账户接口")
@RestController
@RequestMapping("admin/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountAssemble accountAssemble;

    @PostMapping(value = "search",produces = "application/json")
    @ResponseBody
    @ApiOperation("账户列表")
    public PagerResult<AccountVO> queryAccounts(@RequestBody  AccountQuery accountQuery) {
        ListRecordTotalBO<AccountBO> accountListTotalRecord = this.accountService.queryAccount(accountQuery);
        return this.accountAssemble.assemblePagerResult(accountListTotalRecord, accountQuery);
    }

    @PostMapping("enable")
    @ApiOperation("解冻")
    public Integer enableAccount(@RequestParam  String id) throws BusinessException {
        return this.accountService.enableAccount(id);
    }

    @PostMapping("disable")
    @ApiOperation("冻结")
    public Integer disableAccount(@RequestParam String id) throws BusinessException {
        return this.accountService.disableAccount(id);
    }
}