/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.infrastructure.persistence.data.converter;

import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.account.admin.bo.AccountBO;
import com.tarena.tp.luban.account.po.Account;
import com.tarena.tp.luban.account.admin.protocol.param.AccountParam;
import com.tarena.tp.luban.account.admin.protocol.query.AccountQuery;
import com.tarena.tp.luban.account.admin.dao.query.AccountDBPagerQuery;
import org.springframework.beans.BeanUtils;
import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class AccountConverter{

   public AccountDBPagerQuery toDbPagerQuery(AccountQuery accountQuery) {
           if (accountQuery == null) {
               return new AccountDBPagerQuery();
           }
           AccountDBPagerQuery accountDBPagerQuery = new AccountDBPagerQuery();
           BeanUtils.copyProperties(accountQuery, accountDBPagerQuery);
           return accountDBPagerQuery;
       }
   

     public Account param2po(AccountParam param) {
        Account account = new Account();
        BeanUtils.copyProperties(param, account);

        LoginUser loginUser=SecurityContext.getLoginToken();

        account.setGmtCreate(System.currentTimeMillis());
        account.setGmtModified(account.getGmtCreate());
        account.setCreateUserId(loginUser.getUserId());
        account.setModifiedUserId(loginUser.getUserId());
        account.setStatus(1);

        account.setCreateUserName(loginUser.getUserName());
        account.setModifiedUserName(loginUser.getUserName());
        return account;
    }

     public AccountBO po2bo(Account account) {
        AccountBO accountBO = new AccountBO();
        BeanUtils.copyProperties(account, accountBO);
        return accountBO;
    }

     public List<AccountBO> poList2BoList(List<Account> list) {
        List<AccountBO> accountBos = new ArrayList<>(list.size());
        for (Account account : list) {
            accountBos.add(this.po2bo(account));
        }
        return accountBos;
    }

    public void convertStatus(StatusCriteria statusCriteria){
            LoginUser loginUser = SecurityContext.getLoginToken();
            statusCriteria.setModifiedUserId(loginUser.getUserId());
            statusCriteria.setModifiedUserName(loginUser.getUserName());
            statusCriteria.setGmtModified(System.currentTimeMillis());
    }
}