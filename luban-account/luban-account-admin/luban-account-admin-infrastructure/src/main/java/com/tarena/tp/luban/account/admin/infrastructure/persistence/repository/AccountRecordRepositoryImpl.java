/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.infrastructure.persistence.repository;

import com.tarena.tp.luban.account.admin.dao.AccountRecordDAO;
import com.tedu.inn.protocol.enums.StatusRecord;
import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.account.admin.infrastructure.persistence.data.converter.AccountRecordConverter;
import com.tarena.tp.luban.account.po.AccountRecord;
import com.tarena.tp.luban.account.admin.bo.AccountRecordBO;
import com.tarena.tp.luban.account.admin.protocol.param.AccountRecordParam;
import com.tarena.tp.luban.account.admin.repository.AccountRecordRepository;
import com.tarena.tp.luban.account.admin.protocol.query.AccountRecordQuery;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountRecordRepositoryImpl implements AccountRecordRepository {
    @Autowired
    private AccountRecordConverter accountRecordConverter;

    @Autowired
    private AccountRecordDAO accountRecordDao;

    @Override public Long save(AccountRecordParam accountRecordParam) {
        AccountRecord accountRecord = this.accountRecordConverter.param2po(accountRecordParam);
        if (accountRecord.getId() != null) {
            this.accountRecordDao.update(accountRecord);
            return accountRecord.getId();
        }
        this.accountRecordDao.insert(accountRecord);
        return  accountRecord.getId();
    }

    @Override public Integer delete(Long accountRecordId) {
        return this.accountRecordDao.delete(accountRecordId);
    }

    @Override public Integer disable(String accountRecordIds) {
        StatusCriteria statusCriteria = new StatusCriteria(accountRecordIds, StatusRecord.DISABLE.getStatus());
        this.accountRecordConverter.convertStatus(statusCriteria);
        return this.accountRecordDao.changeStatus(statusCriteria);
    }

    @Override public Integer enable(String accountRecordIds) {
        StatusCriteria statusCriteria = new StatusCriteria(accountRecordIds, StatusRecord.ENABLE.getStatus());
        this.accountRecordConverter.convertStatus(statusCriteria);
        return this.accountRecordDao.changeStatus(statusCriteria);
    }

    @Override public AccountRecordBO getAccountRecord(Long accountRecordId) {
        AccountRecord accountRecord = this.accountRecordDao.getEntity(accountRecordId);
        return this.accountRecordConverter.po2bo(accountRecord);
    }

    @Override public List<AccountRecordBO> queryAccountRecords(
        AccountRecordQuery accountRecordQuery) {
        List<AccountRecord> accountRecordList = this.accountRecordDao.queryAccountRecords(this.accountRecordConverter.toDbPagerQuery(accountRecordQuery));
        return this.accountRecordConverter.poList2BoList(accountRecordList);
    }

    @Override public Long getAccountRecordCount(AccountRecordQuery accountRecordQuery) {
        return this.accountRecordDao.countAccountRecord(this.accountRecordConverter.toDbPagerQuery(accountRecordQuery));
    }
}