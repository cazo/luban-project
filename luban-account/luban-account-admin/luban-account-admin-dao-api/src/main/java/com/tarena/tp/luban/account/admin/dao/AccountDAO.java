/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.dao;

import com.tarena.tp.luban.account.po.Account;
import com.tarena.tp.luban.account.admin.dao.query.AccountDBPagerQuery;
import com.tedu.inn.protocol.dao.StatusCriteria;

import java.util.List;

public interface AccountDAO {
    Long insert(Account account);

    Integer delete(Long accountId);

    Integer update(Account account);

    Account getEntity(Long accountId);

    Integer changeStatus(StatusCriteria statusCriteria);


    List<Account> queryAccounts(AccountDBPagerQuery accountPagerQuery);

    Long countAccount(AccountDBPagerQuery accountPagerQuery);

}