/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.po;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(11)")
    /**
     * 主键
     */
    private Long id;

    /**
     * 用户id
     */
    @Column(name = "user_id", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '用户id'", updatable = false)
    private Long userId;

    /**
     * 用户id
     */
    @Column(name = "user_name", columnDefinition = "varchar(16) DEFAULT '' COMMENT '用户名'", updatable = false)
    private String userName;

    /**
     * 用户id
     */
    @Column(name = "user_phone", columnDefinition = "varchar(16) DEFAULT '' COMMENT '用户手机'", updatable = false)
    private String userPhone;


    /**
     * 待结算总额
     */
    @Column(name = "settling_amount", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '待结算总额'", updatable = false)
    private Long settlingAmount;


    /**
     * 结算金额
     */
    @Column(name = "total_amount", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '结算金额'", updatable = false)
    private Long totalAmount;



    /**
     * 创建人名称
     */
    @Column(name = "create_user_name", columnDefinition = "varchar(16) DEFAULT '' COMMENT '创建人'", updatable = false)
    private String createUserName;
    /**
     * 创建人ID
     */
    @Column(name = "create_user_id", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '创建人id'", updatable = false)
    private Long createUserId;
    /**
     * 修改人ID
     */
    @Column(name = "modified_user_id", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '修改人id'")
    private Long modifiedUserId;
    /**
     * 修改人姓名
     */
    @Column(name = "modified_user_name", columnDefinition = "varchar(16)  DEFAULT '' COMMENT '修改人'")
    private String modifiedUserName;
    /**
     * 创建时间
     */
    @Column(name = "gmt_create", columnDefinition = "bigint(20)  DEFAULT 0 COMMENT '创建时间'")
    private Long gmtCreate;
    /**
     * 更新时间
     */
    @Column(name = "gmt_modified", columnDefinition = "bigint(20)  DEFAULT 0 COMMENT '更新时间'")
    private Long gmtModified;
    /**
     * 是否有效状态
     */
    @Column(name = "status", columnDefinition = "tinyint(1)  DEFAULT 0 COMMENT '状态 0:无效，1:有效'")
    private Integer status;
}
